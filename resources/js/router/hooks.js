import Vue from 'vue'

function anyTrue(to, prop) {
  return to.matched.findIndex(item => item.meta[prop]) !== -1
}

// ensure that user signed in
export const ensureSignedIn = router => async (to, from, next) => {
  const requiresAuth = anyTrue(to, 'requiresAuth')

  await Vue.nextTick() // wait for Vue for first rendering

  if (!router.app.user && localStorage.getItem('token')) {
    await router.app.getCurrentUser()
  }

  if (requiresAuth) {
    if (router.app.user) {
      next()
    } else {
      next({ name: 'Login' })
    }
  } else {
    if (router.app.user) {
      next({ name: 'Chat' })
    } else {
      next()
    }
  }
}
