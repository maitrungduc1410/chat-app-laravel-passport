import VueRouter from 'vue-router'
import Vue from 'vue'
import Login from '../components/Login'
import Register from '../components/Register'
import Chat from '../components/Chat'
import { ensureSignedIn } from './hooks'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/login',
      component: Login,
      name: 'Login',
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/register',
      component: Register,
      name: 'Register',
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/chat',
      component: Chat,
      name: 'Chat',
      meta: {
        requiresAuth: true
      }
    }
  ]
})

// router.beforeEach(ensureSession(router))
router.beforeEach(ensureSignedIn(router))


export default router