<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/messages', function (Request $request) {
    return App\Models\Message::with('user')->get();
});

Route::middleware('auth:api')->post('/messages', function (Request $request) {
    $user = Auth::user();

    $message = new App\Models\Message();
    $message->message = request()->get('message', '');
    $message->user_id = $user->id;
    $message->save();

    return ['message' => $message->load('user')];
});

Route::post('/login', function (Request $request) {
    $login_credentials=[
        'email'=>$request->email,
        'password'=>$request->password,
    ];
    if(auth()->attempt($login_credentials)){
        //generate the token for the user
        $user_login_token= auth()->user()->createToken('My Token (login)')->accessToken;
        //now return this token on success login attempt
        return response()->json(['token' => $user_login_token], 200);
    }
    else{
        //wrong login credentials, return, user not authorised to our system, return error code 401
        return response()->json(['error' => 'UnAuthorised Access'], 401);
    }
});

Route::post('/register', function (Request $request) {
    $request->validate([
        'name'=>'required',
        'email'=>'required|email|unique:users',
        'password'=>'required|min:8',
    ]);
    $user= App\Models\User::create([
        'name' =>$request->name,
        'email'=>$request->email,
        'password'=>bcrypt($request->password)
    ]);

    $access_token_example = $user->createToken('My Token (register)')->accessToken;
    //return the access token we generated in the above step
    return response()->json(['token'=>$access_token_example],200);
});